#!/bin/sh
WINE_VERSION=3.21-stage

flatpak install  winehub \
	online.winehub.Platform/i386/18.08 \
	online.winehub.Sdk/i386/18.08

flatpak remove -y online.winehub.Platform.wine.wine-$WINE_VERSION/i386


flatpak-builder -v --user --install -y \
	--arch=i386 --ccache --keep-build-dirs --force-clean \
	--repo=winehub-repo builds/winehub-wine-$WINE_VERSION  \
	winehub-wine/online.winehub.Platform.wine.wine-$WINE_VERSION-32bit.json
