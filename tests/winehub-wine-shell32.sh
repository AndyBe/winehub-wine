#!/bin/sh
flatpak install  winehub \
	online.winehub.Platform/i386/18.08 \
	online.winehub.Sdk/i386/18.08

flatpak remove -y online.winehub.wine.test.shell/i386


flatpak-builder -v --user --install -y \
	--arch=i386 --ccache --keep-build-dirs --force-clean \
	--repo=winehub-repo builds/winehub-wine-test-shell-32bit  \
	winehub-wine/tests/online.winehub.wine.test.shell-32bit.json

flatpak run -v online.winehub.wine.test.shell/i386
