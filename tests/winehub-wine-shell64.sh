#!/bin/sh
flatpak install  winehub \
	online.winehub.Platform.Compat32/x86_64/18.08 \
	online.winehub.Platform/x86_64/18.08 \
	online.winehub.Sdk.Compat32/x86_64/18.08 \
	online.winehub.Sdk/x86_64/18.08

flatpak remove -y winehub.wine.test.shell


flatpak-builder -v --user --install -y \
	--arch=x86_64 --ccache --keep-build-dirs --force-clean \
	--repo=winehub-repo builds/winehub-wine-test-shell-64bit  \
	winehub-wine/tests/online.winehub.wine.test.shell-64bit.json

flatpak run -v online.winehub.wine.test.shell
