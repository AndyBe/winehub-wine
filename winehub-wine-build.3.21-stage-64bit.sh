#!/bin/sh
WINE_VERSION=3.21-stage

flatpak install  winehub \
	online.winehub.Platform/x86_64/18.08 \
	online.winehub.Sdk/x86_64/18.08

flatpak remove -y online.winehub.Platform.wine.wine-$WINE_VERSION/x86_64


flatpak-builder -v --user --install -y \
	--arch=x86_64 --ccache --keep-build-dirs --force-clean \
	--repo=winehub-repo builds/winehub-wine-$WINE_VERSION-64bit  \
	winehub-wine/online.winehub.Platform.wine.wine-$WINE_VERSION-64bit.json
